﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Autofac;
using Albelli.AntonSivov.Modules;
using Autofac.Extensions.DependencyInjection;
using Albelli.AntonSivov.Filters;
using AutoMapper;
using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov
{
    /// <summary>
    /// Startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>The configuration.</value>
        public IConfigurationRoot Configuration { get; }
        /// <summary>
        /// Gets the environment.
        /// </summary>
        /// <value>The environment.</value>
        public IHostingEnvironment Environment { get; }
        /// <summary>
        /// Gets the application container.
        /// </summary>
        /// <value>The application container.</value>
        public IContainer ApplicationContainer { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Albelli.AntonSivov.Startup"/> class.
        /// </summary>
        /// <param name="env">Env.</param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appSettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            Environment = env;
        }

        /// <summary>
        /// Configure the specified app, env and applicationLifeTime.
        /// </summary>
        /// <returns>The configure.</returns>
        /// <param name="app">App.</param>
        /// <param name="env">Env.</param>
        /// <param name="applicationLifeTime">Application life time.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime applicationLifeTime)
        {
            app.UseDeveloperExceptionPage();
            app.UseMvc();
            app.UseSwagger((c) => c.PreSerializeFilters.Add((swagger, httpReq) => swagger.Host = httpReq.Host.Value));
            app.UseSwaggerUI(x =>
            {
                x.RoutePrefix = "swagger/ui";
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            });
            app.UseStaticFiles();

        }

        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <returns>The services.</returns>
        /// <param name="services">Services.</param>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            Mapper.Initialize(cfg=>
            {
                cfg.CreateMissingTypeMaps = false;
                cfg.CreateMap<Contracts.Customer, Models.Customer>();
                cfg.CreateMap<ICustomer, Contracts.Customer>();
                cfg.CreateMap<Contracts.CustomerDetail, Models.CustomerDetail>();
                cfg.CreateMap<ICustomerDetail, Contracts.CustomerDetail>();
                cfg.CreateMap<Contracts.Order, Models.Order>();
                cfg.CreateMap<IOrder, Contracts.Order>();
            });
            services.AddMvc()
                    .AddJsonOptions(options=>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                };
            });

            services.AddSwaggerGen(options => {
                options.SwaggerDoc($"v1", new Swashbuckle.AspNetCore.Swagger.Info()
                {
                    Version = "v1",
                    Title = "Albelli Anton Sivov Test"
                });
                options.DescribeAllEnumsAsStrings();
                options.IncludeXmlComments(Path.ChangeExtension(Assembly.GetEntryAssembly().Location, "xml"));
                options.SchemaFilter<CustomSchemaFilter>();
                options.OperationFilter<CustomOperationFilter>();
            });
            var builder = new ContainerBuilder();
            builder.RegisterModule(new ServiceModule(Configuration));
            builder.Populate(services);
            ApplicationContainer = builder.Build();
            return new AutofacServiceProvider(ApplicationContainer);
        }
    }
}
