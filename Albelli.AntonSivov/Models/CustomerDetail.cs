﻿using System.Collections.Generic;
using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov.Models
{
    /// <summary>
    /// Customer detail.
    /// </summary>
    public class CustomerDetail : Customer, ICustomerDetail
    {
        /// <summary>
        /// Gets or sets the orders.
        /// </summary>
        /// <value>The orders.</value>
        public List<IOrder> Orders { get; set; }

    }
}
