﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Albelli.AntonSivov.Core.Domain;
using Albelli.AntonSivov.Core.Repositories;
using Albelli.AntonSivov.Core.Service;

namespace Albelli.AntonSivov.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;

        public CustomerService(ICustomerRepository customerRepository, IOrderRepository orderRepository)
        {
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
        }

        public async Task<bool> AddNewCustomer(ICustomer customer)
        {
            if(customer == null)
            {
                return false;
            }

            var exCustomer = await _customerRepository.GetCustomerAsync(customer.Email);
            if (exCustomer != null)
            {
                //Customer Exist
                return false;
            }
            return await _customerRepository.SaveCustomer(customer);
        }

        public async Task<bool> AddNewOrder(string customerEmail, IOrder order)
        {
            if(string.IsNullOrEmpty(customerEmail) || order == null)
            {
                return false;
            }

            var exOrder = await _orderRepository.GetOrderAsync(customerEmail, order.OrderId);
            if (exOrder != null)
            {
                //Order Exist
                return false;
            }
            return await _orderRepository.SaveOrder(customerEmail, order);
        }

        public async Task<ICustomer> GetCustomerAsync(string customerEmail)
        {
            return await _customerRepository.GetCustomerAsync(customerEmail);
        }

        public async Task<ICustomerDetail> GetCustomerDetailsAsync(string customerEmail)
        {
            var customer = await _customerRepository.GetCustomerAsync(customerEmail);
            if(customer == null)
            {
                return null;
            }
            return new CustomerDetails(customer, await _orderRepository.GetOrdersByCustomerAsunc(customerEmail));
        }

        public async Task<List<ICustomer>> GetCustomersAsync()
        {
            return await _customerRepository.GetCustomersAsunc();

        }
    }
}
