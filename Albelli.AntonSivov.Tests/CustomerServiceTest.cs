﻿using System;
using Albelli.AntonSivov.Core.Service;
using Albelli.AntonSivov.Services;
using Albelli.AntonSivov.Tests.Mocks;
using Albelli.AntonSivov.Tests.Models;
using Xunit;
namespace Albelli.AntonSivov.Tests
{
    public class CustomerServiceTest
    {
        private readonly ICustomerService _service;

        public CustomerServiceTest()
        {
            _service = new CustomerService(new CustomerRepositoryTest(),
                                           new OrderRepositoryTest());
        }

        [Fact]
        public void GetCustomersTest()
        {
            Assert.Equal(3, _service.GetCustomersAsync().Result.Count);
        }

        [Theory(DisplayName = "GetCustomerTest")]
        [InlineData("test@gmail.com")]
        [InlineData("test1@gmail.com")]
        [InlineData("test2@gmail.com")]
        public void GetCustomerTest(string email)
        {
            Assert.Equal(email, _service.GetCustomerAsync(email).Result.Email);
        }

        [Theory(DisplayName = "GetCustomerTestFault")]
        [InlineData(null)]
        [InlineData("")]
        public void GetCustomerTestFault(string email)
        {
            Assert.Null(_service.GetCustomerAsync(email).Result);
        }

        [Theory(DisplayName = "GetCustomerDetailsTest")]
        [InlineData("test@gmail.com", 3)]
        [InlineData("test1@gmail.com", 1)]
        [InlineData("test2@gmail.com", 0)]
        public void GetCustomerDetailsTest(string email, int count)
        {
            var customer = _service.GetCustomerDetailsAsync(email).Result;
            Assert.Equal(email, customer.Email);
            Assert.Equal(count, customer.Orders.Count);
        }

        [Fact]
        public void AddNewCustomerTest()
        {
            Assert.True(_service.AddNewCustomer(new Customer
            {
                Email = "test3@gmail.com",
                Name = "test name"
            }).Result);
        }

        [Fact]
        public void AddNewCustomerTestFault()
        {
            Assert.False(_service.AddNewCustomer(null).Result);
        }

        [Fact]
        public void AddNewCustomerTestDuplicate()
        {
            Assert.False(_service.AddNewCustomer(new Customer
            {
                Email = "test@gmail.com",
                Name = "test name"
            }).Result);
        }

        [Fact]
        public void AddNewOrderTest()
        {
            Assert.True(_service.AddNewOrder("test@gmail.com",
                                             new Order
            {
                OrderId = Guid.NewGuid().ToString(),
                Price = 100,
                CreatedDate = DateTime.UtcNow
            }).Result);
        }



        [Fact]
        public void AddNewOrderTestCustomerNotFound()
        {
            Assert.False(_service.AddNewOrder("test3@gmail.com",
                                             new Order
            {
                OrderId = Guid.NewGuid().ToString(),
                Price = 100,
                CreatedDate = DateTime.UtcNow
            }).Result);
        }

        [Fact]
        public void AddNewOrderTestCustomerNull()
        {
            Assert.False(_service.AddNewOrder(null,
                                             new Order
                                             {
                                                 OrderId = Guid.NewGuid().ToString(),
                                                 Price = 100,
                                                 CreatedDate = DateTime.UtcNow
                                             }).Result);
        }

        [Fact]
        public void AddNewOrderTestOrderNull()
        {
            Assert.False(_service.AddNewOrder("test3@gmail.com",
                                             null).Result);
        }

       
    }
}
