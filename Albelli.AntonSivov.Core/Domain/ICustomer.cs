﻿using System;
namespace Albelli.AntonSivov.Core.Domain
{
    /// <summary>
    /// Customer.
    /// </summary>
    public interface ICustomer
    {
        /// <summary>
        /// The email of customer. Unique for customers
        /// </summary>
        string Email { get; set; }
        /// <summary>
        /// The name of customer
        /// </summary>
        string Name { get; set; }
    }
}
