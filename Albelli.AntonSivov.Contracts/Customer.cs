﻿using System;
namespace Albelli.AntonSivov.Contracts
{
    public class Customer
    {
        /// <summary>
        /// The email of customer. Unique for customers
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// The name of customer
        /// </summary>
        public string Name { get; set; }
    }
}
