// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace Albelli.AntonSivov.Client.AutorestClient
{
    using Models;
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Extension methods for AlbelliAntonSivovTest.
    /// </summary>
    public static partial class AlbelliAntonSivovTestExtensions
    {
            /// <summary>
            /// Gets all customers.
            /// </summary>
            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            public static IList<Customer> ApiCustomerGet(this IAlbelliAntonSivovTest operations)
            {
                return operations.ApiCustomerGetAsync().GetAwaiter().GetResult();
            }

            /// <summary>
            /// Gets all customers.
            /// </summary>
            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='cancellationToken'>
            /// The cancellation token.
            /// </param>
            public static async Task<IList<Customer>> ApiCustomerGetAsync(this IAlbelliAntonSivovTest operations, CancellationToken cancellationToken = default(CancellationToken))
            {
                using (var _result = await operations.ApiCustomerGetWithHttpMessagesAsync(null, cancellationToken).ConfigureAwait(false))
                {
                    return _result.Body;
                }
            }

            /// <summary>
            /// Adds the customer.
            /// </summary>
            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='customer'>
            /// Customer.
            /// </param>
            public static object ApiCustomerPost(this IAlbelliAntonSivovTest operations, Customer customer)
            {
                return operations.ApiCustomerPostAsync(customer).GetAwaiter().GetResult();
            }

            /// <summary>
            /// Adds the customer.
            /// </summary>
            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='customer'>
            /// Customer.
            /// </param>
            /// <param name='cancellationToken'>
            /// The cancellation token.
            /// </param>
            public static async Task<object> ApiCustomerPostAsync(this IAlbelliAntonSivovTest operations, Customer customer, CancellationToken cancellationToken = default(CancellationToken))
            {
                using (var _result = await operations.ApiCustomerPostWithHttpMessagesAsync(customer, null, cancellationToken).ConfigureAwait(false))
                {
                    return _result.Body;
                }
            }

            /// <summary>
            /// Gets the customer.
            /// </summary>
            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='customerEmail'>
            /// Customer email.
            /// </param>
            public static object ApiCustomerByCustomerEmailGet(this IAlbelliAntonSivovTest operations, string customerEmail)
            {
                return operations.ApiCustomerByCustomerEmailGetAsync(customerEmail).GetAwaiter().GetResult();
            }

            /// <summary>
            /// Gets the customer.
            /// </summary>
            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='customerEmail'>
            /// Customer email.
            /// </param>
            /// <param name='cancellationToken'>
            /// The cancellation token.
            /// </param>
            public static async Task<object> ApiCustomerByCustomerEmailGetAsync(this IAlbelliAntonSivovTest operations, string customerEmail, CancellationToken cancellationToken = default(CancellationToken))
            {
                using (var _result = await operations.ApiCustomerByCustomerEmailGetWithHttpMessagesAsync(customerEmail, null, cancellationToken).ConfigureAwait(false))
                {
                    return _result.Body;
                }
            }

            /// <summary>
            /// Adds the order.
            /// </summary>
            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='customerEmail'>
            /// Customer email.
            /// </param>
            /// <param name='order'>
            /// Order.
            /// </param>
            public static object ApiCustomerByCustomerEmailAddorderPost(this IAlbelliAntonSivovTest operations, string customerEmail, Order order)
            {
                return operations.ApiCustomerByCustomerEmailAddorderPostAsync(customerEmail, order).GetAwaiter().GetResult();
            }

            /// <summary>
            /// Adds the order.
            /// </summary>
            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='customerEmail'>
            /// Customer email.
            /// </param>
            /// <param name='order'>
            /// Order.
            /// </param>
            /// <param name='cancellationToken'>
            /// The cancellation token.
            /// </param>
            public static async Task<object> ApiCustomerByCustomerEmailAddorderPostAsync(this IAlbelliAntonSivovTest operations, string customerEmail, Order order, CancellationToken cancellationToken = default(CancellationToken))
            {
                using (var _result = await operations.ApiCustomerByCustomerEmailAddorderPostWithHttpMessagesAsync(customerEmail, order, null, cancellationToken).ConfigureAwait(false))
                {
                    return _result.Body;
                }
            }

    }
}
