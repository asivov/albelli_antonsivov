﻿using System.Collections.Generic;
using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov.Services
{
    internal class CustomerDetails : ICustomerDetail
    {

        public List<IOrder> Orders { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }

        public CustomerDetails(ICustomer customer, List<IOrder> list)
        {
            Name = customer.Name;
            Email = customer.Email;
            Orders = list;
        }
       
    }
}