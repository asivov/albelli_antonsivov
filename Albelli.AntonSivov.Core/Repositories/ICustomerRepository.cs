﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov.Core.Repositories
{
    /// <summary>
    /// Customer repository. Provide access to DB
    /// </summary>
    public interface ICustomerRepository
    {
        /// <summary>
        /// Gets the customer async.
        /// </summary>
        /// <returns>The customer async.</returns>
        /// <param name="customerEmail">Customer email.</param>
        Task<ICustomer> GetCustomerAsync(string customerEmail);

        /// <summary>
        /// Gets the customers asunc.
        /// </summary>
        /// <returns>The customers asunc.</returns>
        Task<List<ICustomer>> GetCustomersAsunc();

        /// <summary>
        /// Saves the customer.
        /// </summary>
        /// <returns>If operation success</returns>
        /// <param name="customer">Customer.</param>
        Task<bool> SaveCustomer(ICustomer customer);

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <returns>If operation success</returns>
        /// <param name="customerEmail">Customer email.</param>
        Task<bool> DeleteCustomer(string customerEmail);
    }
}
