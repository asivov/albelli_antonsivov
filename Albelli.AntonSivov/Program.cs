﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace Albelli.AntonSivov
{
    sealed class Program
    {
        static async Task Main(string[] args)
        {

#if DEBUG
            Console.WriteLine("Is Debug");
#else
            Console.WriteLine("Is Release");
#endif
            try
            {
                var host = new WebHostBuilder()
                    .UseKestrel()
                    .UseUrls("http://*:5000")
                    .UseStartup<Startup>()
                    .Build();

                await host.RunAsync();
            }
            catch(Exception ex)
            {
                var delay = TimeSpan.FromMinutes(1);
                Console.WriteLine("Application Error");
                Console.WriteLine(ex);

                Console.WriteLine();
                Console.WriteLine($"Process will be terminated in {delay}. Press any key to terminate immediately.");

                await Task.WhenAny(
                    Task.Delay(delay),
                    Task.Run(()=>{Console.ReadKey(true);})
                );
            }

            Console.WriteLine("Application terminated");
        }
    }
}
