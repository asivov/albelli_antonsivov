﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Albelli.AntonSivov.Contracts;

namespace Albelli.AntonSivov.Client
{
    public interface IAlbelliProxyClient
    {
        Task<List<Customer>> GetAllCustomers();

        Task<CustomerDetail> GetCustomer(string customerEmail);

        Task AddCustomer(Customer customer);

        Task AddOrder(string customerEmail, Order order);
    }
}
