﻿using System;
using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov.Models
{
    /// <summary>
    /// Order.
    /// </summary>
    public class Order : IOrder
    {
        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>The order identifier.</value>
        public string OrderId { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>The price.</value>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>The created date.</value>
        public DateTime CreatedDate { get; set; }
    }
}
