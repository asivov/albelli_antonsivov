﻿using System;
using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov.Models
{
    /// <summary>
    /// Customer.
    /// </summary>
    public class Customer : ICustomer
    {
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
    }
}
