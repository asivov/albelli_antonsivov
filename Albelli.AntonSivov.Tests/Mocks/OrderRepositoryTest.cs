﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albelli.AntonSivov.Core.Domain;
using Albelli.AntonSivov.Core.Repositories;

namespace Albelli.AntonSivov.Tests.Mocks
{
    internal class OrderRepositoryTest : IOrderRepository
    {
        private readonly Dictionary<string, List<IOrder>> _orders;

        public OrderRepositoryTest()
        {
            _orders = new Dictionary<string, List<IOrder>>
            {
                {"test@gmail.com", new List<IOrder>{
                        new Order{OrderId = Guid.NewGuid().ToString(), Price = 100, CreatedDate = DateTime.Now},
                        new Order{OrderId = Guid.NewGuid().ToString(), Price = 50, CreatedDate = DateTime.Now},
                        new Order{OrderId = Guid.NewGuid().ToString(), Price = 300, CreatedDate = DateTime.Now}
                    }},
                {"test1@gmail.com", new List<IOrder>{
                        new Order{OrderId = Guid.NewGuid().ToString(), Price = 100, CreatedDate = DateTime.Now}
                    }},
                {"test2@gmail.com", new List<IOrder>()}

            };
        }
        public async Task<bool> DeleteOrder(string customerEmail, string orderId)
        {
            return _orders.Keys.Any(k=>k.Equals(customerEmail));
        }

        public async Task<IOrder> GetOrderAsync(string customerEmail, string orderId)
        {
            if (!_orders.Keys.Contains(customerEmail))
            {
                return null;
            }
            return _orders[customerEmail].FirstOrDefault(o => o.OrderId.Equals(orderId));
        }

        public async Task<List<IOrder>> GetOrdersByCustomerAsunc(string customerEmail)
        {
            if(!_orders.Keys.Contains(customerEmail))
            {
                return new List<IOrder>();
            }
            return _orders[customerEmail];
        }

        public async Task<bool> SaveOrder(string customerEmail, IOrder order)
        {
            return _orders.Keys.Any(k => k.Equals(customerEmail));
        }
    }
}