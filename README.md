# README #

The application uses .Net Core 2.0 and MS Storage like a DB.

## What is this repository for? ##

This is a test project and ahould realise the followitng requarements:

### Description ###
Create a .NET REST API serving the domain of Customers and Orders.
The code should have reasonable Unit-tests coverage.
### Domain ###
Customer has Name and Email
Customer can have multiple Orders
Order can belong to only one Customer
Order has at least two fields: Price and CreatedDate.
### API ###
The application should at least expose these endpoints which do the following:

* Return a list of customers, without orders
* Return a customer and his orders
* Add a new Order for an existing Customer
* Add a new Customer