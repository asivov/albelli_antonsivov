﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using System.Collections.Generic;

namespace Albelli.AntonSivov.Filters
{
    /// <summary>
    /// Custom operation filter.
    /// </summary>
    public class CustomOperationFilter : IOperationFilter
    {
        /// <summary>
        /// Apply the specified operation and context.
        /// </summary>
        /// <returns>The apply.</returns>
        /// <param name="operation">Operation.</param>
        /// <param name="context">Context.</param>
        public void Apply(Operation operation, OperationFilterContext context)
        {
            operation.Produces.Clear();
            operation.Produces.Add("application/json");

            operation.Consumes.Clear();
            operation.Consumes.Add("application/json");

            if(operation.Parameters == null || !operation.Parameters.Any())
            {
                return;
            }

            SetBodyParametersAsRequared(operation);
        }

        /// <summary>
        /// Sets the body parameters as requared.
        /// </summary>
        /// <param name="operation">Operation.</param>
        public void SetBodyParametersAsRequared(Operation operation)
        {
            IEnumerable<IParameter> bodyParameters = operation.Parameters.Where(p => p.In == "body");
            {

                foreach(IParameter bodyParameter  in bodyParameters)
                {
                    bodyParameter.Required = true;
                }
            }
        }
    }
}