﻿using System;
using Albelli.AntonSivov.AzureRepositories;
using Albelli.AntonSivov.Core.Repositories;
using Albelli.AntonSivov.Core.Service;
using Albelli.AntonSivov.Services;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Albelli.AntonSivov.Modules
{
    /// <summary>
    /// Service module.
    /// </summary>
    public class ServiceModule : Module
    {
        private readonly IServiceCollection _services;
        private readonly IConfigurationRoot _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Albelli.AntonSivov.Modules.ServiceModule"/> class.
        /// </summary>
        /// <param name="configuration">Configuration.</param>
        public ServiceModule(IConfigurationRoot configuration)
        {
            _services = new ServiceCollection();
            _configuration = configuration;
        }


        /// <summary>
        /// Load the specified builder.
        /// </summary>
        /// <returns>The load.</returns>
        /// <param name="builder">Builder.</param>
        protected override void Load(ContainerBuilder builder)
        {
            var dbConnString = _configuration.GetValue<string>("DbConnString");
            var customerRepository = new CustomerRepository(dbConnString);
            builder.RegisterInstance<ICustomerRepository>(customerRepository).SingleInstance();

            var orderRepository = new OrderRepository(dbConnString);
            builder.RegisterInstance<IOrderRepository>(orderRepository).SingleInstance();

            builder.RegisterType<CustomerService>().As<ICustomerService>();

            builder.Populate(_services);
        }
    }
}
