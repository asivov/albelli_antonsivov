﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Albelli.AntonSivov.Filters
{
    /// <summary>
    /// Custom schema filter.
    /// </summary>
    public class CustomSchemaFilter : ISchemaFilter
    {
        /// <summary>
        /// Apply the specified model and context.
        /// </summary>
        /// <returns>The apply.</returns>
        /// <param name="model">Model.</param>
        /// <param name="context">Context.</param>
        public void Apply(Schema model, SchemaFilterContext context)
        {
          if (model.Properties == null)
            return;
                PropertyInfo[] publicProperties = GetPublicProperties(context.SystemType);
          foreach (KeyValuePair<string, Schema> keyValuePair in model.Properties.Where<KeyValuePair<string, Schema>>((Func<KeyValuePair<string, Schema>, bool>) (x => x.Value.Enum != null)))
          {
            KeyValuePair<string, Schema> property = keyValuePair;
            PropertyInfo propertyInfo = ((IEnumerable<PropertyInfo>) publicProperties).SingleOrDefault<PropertyInfo>((Func<PropertyInfo, bool>) (p => string.Equals(p.Name, property.Key, StringComparison.OrdinalIgnoreCase)));
            if (propertyInfo == (PropertyInfo) null)
              throw new InvalidOperationException(string.Format("Property {0} not found in type {1}", (object) property.Key, (object) context.SystemType));

          }
        }
     
        private static PropertyInfo[] GetPublicProperties(Type type)
        {
          if (!type.GetTypeInfo().IsInterface)
            return type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
          List<PropertyInfo> propertyInfos = new List<PropertyInfo>();
          List<Type> typeList = new List<Type>();
          Queue<Type> typeQueue = new Queue<Type>();
          typeList.Add(type);
          typeQueue.Enqueue(type);
          while (typeQueue.Count > 0)
          {
            Type type1 = typeQueue.Dequeue();
            foreach (Type type2 in type1.GetInterfaces())
            {
              if (!typeList.Contains(type2))
              {
                typeList.Add(type2);
                typeQueue.Enqueue(type2);
              }
            }
            IEnumerable<PropertyInfo> collection = ((IEnumerable<PropertyInfo>) type1.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy)).Where<PropertyInfo>((Func<PropertyInfo, bool>) (x => !propertyInfos.Contains(x)));
            propertyInfos.InsertRange(0, collection);
          }
          return propertyInfos.ToArray();
        }
 
    }
}