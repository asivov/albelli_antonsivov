﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albelli.AntonSivov.AzureRepositories.Tables;
using Albelli.AntonSivov.Core.Domain;
using Albelli.AntonSivov.Core.Repositories;
namespace Albelli.AntonSivov.AzureRepositories
{
    public class CustomerRepository : BaseStorage<Customer>, ICustomerRepository
    {
        public CustomerRepository(string connectionString) : base(connectionString, "Customers")
        {
        }

        public async Task<bool> DeleteCustomer(string customerId)
        {
            return await DeleteIfExistAsync(Customer.GetPartitionName(), customerId);
        }

        public async Task<ICustomer> GetCustomerAsync(string customerId)
        {
            return await GetDataAsync(Customer.GetPartitionName(), customerId);
        }

        public async Task<List<ICustomer>> GetCustomersAsunc()
        {
            var result = await GetDataAsync();
            return result.Cast<ICustomer>().ToList();
        }

        public async Task<bool> SaveCustomer(ICustomer customer)
        {
            try{
                await InsertOrMergeAsync(new Customer(customer){PartitionKey = Customer.GetPartitionName()});
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
