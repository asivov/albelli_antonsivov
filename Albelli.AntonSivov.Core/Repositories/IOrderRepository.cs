﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov.Core.Repositories
{
    public interface IOrderRepository
    {
        /// <summary>
        /// Gets the order async.
        /// </summary>
        /// <returns>The order async.</returns>
        /// <param name="customerEmail">Customer email.</param>
        /// <param name="orderId">Order identifier.</param>
        Task<IOrder> GetOrderAsync(string customerEmail, string orderId);

        /// <summary>
        /// Gets the orders by customer asunc.
        /// </summary>
        /// <returns>The orders by customer asunc.</returns>
        /// <param name="customerEmail">Customer email.</param>
        Task<List<IOrder>> GetOrdersByCustomerAsunc(string customerEmail);

        /// <summary>
        /// Saves the order.
        /// </summary>
        /// <returns>Success operation</returns>
        /// <param name="customerEmail">Customer email.</param>
        /// <param name="order">Order.</param>
        Task<bool> SaveOrder(string customerEmail, IOrder order);

        /// <summary>
        /// Deletes the order.
        /// </summary>
        /// <returns>Success operation</returns>
        /// <param name="customerId">Customer identifier.</param>
        /// <param name="orderId">Order identifier.</param>
        Task<bool> DeleteOrder(string customerId, string orderId);
    }
}
