﻿using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov.Tests.Models
{
    internal class Customer : ICustomer
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }
}