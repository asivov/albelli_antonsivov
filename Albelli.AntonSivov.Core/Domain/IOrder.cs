﻿using System;
namespace Albelli.AntonSivov.Core.Domain
{
    /// <summary>
    /// Order.
    /// </summary>
    public interface IOrder
    {
        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>The order identifier.</value>
        string OrderId { get; set; }
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>The price.</value>
        decimal Price { get; set; }
        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>The created date.</value>
        DateTime CreatedDate { get; set; }
    }
}
