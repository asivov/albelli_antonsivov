﻿using System;
using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov.Tests
{
    internal class Order : IOrder
    {
        public string OrderId { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}