﻿using System.Collections.Generic;

namespace Albelli.AntonSivov.Contracts
{
    public class CustomerDetail : Customer
    {
        public List<Order> Orders { get; set; }
    }
}