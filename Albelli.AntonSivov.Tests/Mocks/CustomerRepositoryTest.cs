﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albelli.AntonSivov.Core.Domain;
using Albelli.AntonSivov.Core.Repositories;
using Albelli.AntonSivov.Tests.Models;

namespace Albelli.AntonSivov.Tests.Mocks
{
    internal class CustomerRepositoryTest : ICustomerRepository
    {
        private readonly List<ICustomer> _customers;
        public CustomerRepositoryTest()
        {
            _customers = new List<ICustomer>
            {
                new Customer {Email="test@gmail.com", Name="Test"},
                new Customer {Email="test1@gmail.com", Name="Test1"},
                new Customer {Email="test2@gmail.com", Name="Test2"}
            };
        }

        public async Task<bool> DeleteCustomer(string customerEmail)
        {
            // do nothing
            return true;
        }

        public async Task<ICustomer> GetCustomerAsync(string customerEmail)
        {
            return _customers.FirstOrDefault(c=>c.Email.Equals(customerEmail));
        }

        public async Task<List<ICustomer>> GetCustomersAsunc()
        {
            return _customers;
        }

        public async Task<bool> SaveCustomer(ICustomer customer)
        {
            //Do nothing
            return true;
        }
    }
}