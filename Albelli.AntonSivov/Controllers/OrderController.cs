﻿using System;
using Microsoft.AspNetCore.Mvc;
using Albelli.AntonSivov.Core.Service;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.SwaggerGen;
using AutoMapper;
using Albelli.AntonSivov.Contracts;
using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov.Controllers
{
    /// <summary>
    /// Order controller.
    /// </summary>
    [Route("/api/order")]
    public class OrderController : Controller
    {
        private readonly ICustomerService _customerService;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Albelli.AntonSivov.Controllers.OrderController"/> class.
        /// </summary>
        /// <param name="customerService">Customer service.</param>
        public OrderController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Adds the order.
        /// </summary>
        /// <returns>Nothins.</returns>
        /// <param name="order">Order.</param>
        /// <param name="customerEmail">Customer email.</param>
        [SwaggerResponse(200, typeof(object), "Save succesful")]
        [SwaggerResponse(400, typeof(object), "Internal error")]
        [SwaggerResponse(404, typeof(object), "Customer was not found")]
        [HttpPost("/api/customer/{customerEmail}/addorder")]
        public async Task<IActionResult> AddOrder(string customerEmail, [FromBody] Contracts.Order order)
        {
            var customer = await _customerService.GetCustomerAsync(customerEmail);
            if(customer == null)
            {
                return NotFound(new object());
            }

            var result = await _customerService.AddNewOrder(customerEmail, Mapper.Map<Models.Order>(order));
            return result ? (IActionResult)Ok(new object()) : BadRequest(new object());
        }
    }
}
