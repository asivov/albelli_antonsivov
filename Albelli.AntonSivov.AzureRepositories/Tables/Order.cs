﻿using System;
using System.Globalization;
using Albelli.AntonSivov.Core.Domain;
using Microsoft.WindowsAzure.Storage.Table;

namespace Albelli.AntonSivov.AzureRepositories.Tables
{
    public class Order : TableEntity, IOrder
    {
        public Order()
        {
        }

        public Order(string customerEmail, IOrder order)
        {
            OrderId = order.OrderId;
            Price = order.Price;
            CreatedDate = order.CreatedDate;
            CustomerEmail = customerEmail;
            ETag = "*";
        }

        public string OrderId { get => RowKey; set => RowKey = value; }
        public string CustomerEmail { get => PartitionKey; set => PartitionKey = value; }
        [IgnoreProperty]
        public decimal Price { get; set; }
        public string SPrice
        {
            get => Price.ToString(CultureInfo.InvariantCulture);
            set
            {
                decimal result;
                if (decimal.TryParse(value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out result))
                {
                    Price = result;
                }
                else
                {
                    Price = 0;
                }

            }
        }
        [IgnoreProperty]
        public DateTime CreatedDate { get; set; }
        public string SCreatedDate {
            get => CreatedDate.ToString(CultureInfo.InvariantCulture);
            set{
                DateTime result;
                if(DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
                {
                    CreatedDate = result;
                }
                else
                {
                    CreatedDate = new DateTime();
                }

            }
        }
    }
}
