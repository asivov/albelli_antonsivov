﻿using System;
using System.Collections.Generic;

namespace Albelli.AntonSivov.Core.Domain
{
    /// <summary>
    /// Customer detail.
    /// </summary>
    public interface ICustomerDetail : ICustomer
    {
        /// <summary>
        /// List of orders for the customer
        /// </summary>
        List<IOrder> Orders { get; set; }
    }
}
