﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Albelli.AntonSivov.Core.Domain;

namespace Albelli.AntonSivov.Core.Service
{
    /// <summary>
    /// Customer service.
    /// </summary>
    public interface ICustomerService
    {
        /// <summary>
        /// Gets the customers async.
        /// </summary>
        /// <returns>The customers async.</returns>
        Task<List<ICustomer>> GetCustomersAsync();

        /// <summary>
        /// Gets the customer async.
        /// </summary>
        /// <param name="customerEmail">Customer email.</param>
        /// <returns>The customer details async.</returns>
        Task<ICustomer> GetCustomerAsync(string customerEmail);

        /// <summary>
        /// Gets the customer details async.
        /// </summary>
        /// <param name="customerEmail">Customer email.</param>
        /// <returns>The customer details async.</returns>
        Task<ICustomerDetail> GetCustomerDetailsAsync(string customerEmail);

        /// <summary>
        /// Adds the new customer.
        /// </summary>
        /// <returns>The new customer.</returns>
        /// <param name="customer">Customer.</param>
        Task<bool> AddNewCustomer(ICustomer customer);

        /// <summary>
        /// Adds the new order.
        /// </summary>
        /// <returns>The new order.</returns>
        /// <param name="customerEmail">Customer email.</param>
        /// <param name="order">Order.</param>
        Task<bool> AddNewOrder(string customerEmail, IOrder order);
    }
}

