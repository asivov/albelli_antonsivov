﻿using Albelli.AntonSivov.Core.Domain;
using Microsoft.WindowsAzure.Storage.Table;

namespace Albelli.AntonSivov.AzureRepositories.Tables
{
    public class Customer : TableEntity, ICustomer
    {
        public Customer()
        {
        }

        public Customer(ICustomer customer)
        {
            Email = customer.Email;
            Name = customer.Name;
            ETag = "*";
        }

        public static string GetPartitionName()
        {
            return "C";
        }

        public string Email { get => RowKey; set => RowKey = value; }
        public string Name { get; set; }
    }
}
