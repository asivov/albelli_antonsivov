﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albelli.AntonSivov.AzureRepositories.Tables;
using Albelli.AntonSivov.Core.Domain;
using Albelli.AntonSivov.Core.Repositories;
namespace Albelli.AntonSivov.AzureRepositories
{
    public class OrderRepository : BaseStorage<Order>, IOrderRepository
    {
        public OrderRepository(string connectionString) : base(connectionString, "Orders")
        {
            
        }

        public async Task<bool> DeleteOrder(string customerId, string orderId)
        {
            return await DeleteIfExistAsync(customerId, orderId);
        }

        public async Task<IOrder> GetOrderAsync(string customerId, string orderId)
        {
            return await GetDataAsync(customerId, orderId);
        }

        public async Task<List<IOrder>> GetOrdersByCustomerAsunc(string customerId)
        {
            var result = await GetDataAsync(customerId);
            return result.Cast<IOrder>().ToList();
        }

        public async Task<bool> SaveOrder(string customerEmail, IOrder order)
        {
            try
            {
                await InsertOrMergeAsync(new Order(customerEmail, order));
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
