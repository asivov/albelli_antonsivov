﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Albelli.AntonSivov.AzureRepositories
{
    public class BaseStorage<T> where T : class, ITableEntity, new()
    {

        private bool _tableCreated = false;
        private readonly string _tableName;
        private readonly CloudStorageAccount _cloudStorageAccount;
        private readonly TimeSpan _maxExecutionTime;

        public BaseStorage(string connectionString, string tableName, TimeSpan? maxExecutionTime = null)
        {
            _tableName = tableName;
            _cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
            _maxExecutionTime = maxExecutionTime.GetValueOrDefault(TimeSpan.FromSeconds(10));
        }
        
        private async Task<CloudTable>GetTableAsync()
        {
            if(_tableCreated)
            {
                return GetTableReference();
            }

            return await CreateAndGetTableIfNotExistsAsync();
        }

        private CloudTable GetTableReference()
        {
            var cloudTableClient = _cloudStorageAccount.CreateCloudTableClient();
            return cloudTableClient.GetTableReference(_tableName);
        }

        private async Task<CloudTable> CreateAndGetTableIfNotExistsAsync()
        {
            var table = GetTableReference();
            await table.CreateIfNotExistsAsync();
            _tableCreated = true;
            return table;
        }


        private TableRequestOptions GetRequestOptions()
        {
            return new TableRequestOptions
            {
                MaximumExecutionTime = _maxExecutionTime
            };
        }
        private static OperationContext GetMergeOperationContext()
        {
            return new OperationContext
            {
                UserHeaders = new Dictionary<string, string>
                {
                    {"MergingOperation", string.Empty}
                }
            };
        }

        private static IEnumerable<N> ApplyFilter<N>(IEnumerable<N> data, Func<N, bool> filter) 
        {
            return filter == null ? data : data.Where(filter);
        }

        private async Task ExecuteQueryAsync(TableQuery<T> rangeQuery, Func<T, bool> filter,
            Func<IEnumerable<T>, Task> yieldData)
        {
            TableContinuationToken tableContinuationToken = null;
            var table = await GetTableAsync();
            do
            {
                var queryResponse = await table.ExecuteQuerySegmentedAsync(rangeQuery, tableContinuationToken, GetRequestOptions(), null);
                tableContinuationToken = queryResponse.ContinuationToken;
                await yieldData(ApplyFilter(queryResponse.Results, filter));
            } while (tableContinuationToken != null);
        }

        private TableQuery<T> CompileTableQuery(string partition)
        {
            var filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partition);
            return new TableQuery<T>().Where(filter);
        }

        protected async Task InsertOrMergeAsync(T item)
        {
            var table = await GetTableAsync();
            await table.ExecuteAsync(TableOperation.InsertOrMerge(item), GetRequestOptions(), GetMergeOperationContext());
        }

        protected virtual async Task DeleteAsync(T item)
        {
            var table = await GetTableAsync();
            await table.ExecuteAsync(TableOperation.Delete(item), GetRequestOptions(), null);
        }

        protected async Task<T> DeleteAsync(string partitionKey, string rowKey)
        {
            var itm = await GetDataAsync(partitionKey, rowKey);
            if (itm != null)
                await DeleteAsync(itm);
            return itm;
        }

        protected async Task<bool> DeleteIfExistAsync(string partitionKey, string rowKey)
        {
            try
            {
                await DeleteAsync(partitionKey, rowKey);
            }
            catch (StorageException ex)
            {
                if (ex.RequestInformation.HttpStatusCode == 404)
                    return false;

                throw;
            }

            return true;
        }

        protected async Task<bool> DeleteAsync()
        {
            bool deleted;

            try
            {
                var table = await GetTableAsync();

                deleted = await table.DeleteIfExistsAsync();

                if (deleted)
                {
                    _tableCreated = false;
                }
            }
            catch (StorageException ex)
            {
                if (ex.RequestInformation.HttpStatusCode == 404)
                    return false;

                throw;
            }

            return deleted;
        }

        public virtual async Task<T> GetDataAsync(string partition, string row)
        {
            var retrieveOperation = TableOperation.Retrieve<T>(partition, row);
            var table = await GetTableAsync();
            var retrievedResult = await table.ExecuteAsync(retrieveOperation, GetRequestOptions(), null);
            return (T)retrievedResult.Result;
        }

        public virtual async Task<IEnumerable<T>> GetDataAsync(string partition, Func<T, bool> filter = null)
        {
            var rangeQuery = CompileTableQuery(partition);
            var result = new List<T>();

            await ExecuteQueryAsync(rangeQuery, filter, itms =>
            {
                result.AddRange(itms);
                return Task.FromResult(true);
            });

            return result;
        }

        public async Task<IList<T>> GetDataAsync(Func<T, bool> filter = null)
        {
            var rangeQuery = new TableQuery<T>();
            var result = new List<T>();
            await ExecuteQueryAsync(rangeQuery, filter, itms =>
            {
                result.AddRange(itms);
                return Task.FromResult(true);
            });
            return result;
        }


    }
}
