﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albelli.AntonSivov.Client.AutorestClient;
using Albelli.AntonSivov.Contracts;

namespace Albelli.AntonSivov.Client
{
    public class AlbelliProxyClient : IAlbelliProxyClient, IDisposable
    {
        private IAlbelliAntonSivovTest _client;

        public AlbelliProxyClient(string serviceUrl)
        {
            _client = new AlbelliAntonSivovTest(new Uri(serviceUrl));
        }


        public async Task<List<Customer>> GetAllCustomers()
        {
            var result = await _client.ApiCustomerGetWithHttpMessagesAsync();
            if (result.Response.IsSuccessStatusCode)
            {
                return result.Body.Select(r => new Customer
                {
                    Email = r.Email,
                    Name = r.Name
                }).ToList();
            }
            return null;
        }

        public async Task<CustomerDetail> GetCustomer(string customerEmail)
        {
            var result = await _client.ApiCustomerByCustomerEmailGetWithHttpMessagesAsync(customerEmail);
            if (result.Response.IsSuccessStatusCode)
            {
                var body = (CustomerDetail)result.Body;
                return new CustomerDetail
                {
                    Email = body.Email,
                    Name = body.Name,
                    Orders = new List<Order>(
                        from o in body.Orders
                        select new Order
                        {
                            OrderId = o.OrderId,
                            Price = o.Price,
                            CreatedDate = o.CreatedDate
                        }
                        )
                };
            }
            return null;
        }

        public async Task AddCustomer(Customer customer)
        {
            await _client.ApiCustomerPostWithHttpMessagesAsync(new AutorestClient.Models.Customer
            {
                Email = customer.Email,
                Name = customer.Name
            });
        }

        public async Task AddOrder(string customerEmail, Order order)
        {
            await _client.ApiCustomerByCustomerEmailAddorderPostWithHttpMessagesAsync(customerEmail,
                new AutorestClient.Models.Order
                {
                    CreatedDate = order.CreatedDate,
                    OrderId = order.OrderId,
                    Price = (double?)order.Price
                });
        }

        public void Dispose()
        {
            if (_client != null)
            {
                _client.Dispose();
                _client = null;
            }
        }
    }
}
