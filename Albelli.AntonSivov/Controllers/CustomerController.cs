﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Albelli.AntonSivov.Contracts;
using Albelli.AntonSivov.Core.Domain;
using Albelli.AntonSivov.Core.Service;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Albelli.AntonSivov.Controllers
{
    /// <summary>
    /// Customer controller.
    /// </summary>
    [Route("/api/customer")]
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Albelli.AntonSivov.Controllers.CustomerController"/> class.
        /// </summary>
        /// <param name="customerService">Customer service.</param>
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <returns>The all customers.</returns>
        [SwaggerResponse(200, typeof(List<Contracts.Customer>))]
        [HttpGet("")]
        public async Task<IActionResult> GetAllCustomers()
        {
            var result = await _customerService.GetCustomersAsync();
            return Ok(result.Select(c=>Mapper.Map<Contracts.Customer>(c)));
        }

        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <returns>The customer.</returns>
        /// <param name="customerEmail">Customer email.</param>
        [SwaggerResponse(200, typeof(Contracts.CustomerDetail))]
        [SwaggerResponse(404, typeof(object), "Customer not found")]
        [HttpGet("{customerEmail}")]
        public async Task<IActionResult> GetCustomer(string customerEmail)
        {
            var result = await _customerService.GetCustomerDetailsAsync(customerEmail);
            if(result == null)
            {
                return NotFound(new object());
            }
            return Ok(Mapper.Map<Contracts.CustomerDetail>(result));
        }


        /// <summary>
        /// Adds the customer.
        /// </summary>
        /// <returns>The customer.</returns>
        /// <param name="customer">Customer.</param>
        [SwaggerResponse(200, typeof(object), "Save succesful")]
        [SwaggerResponse(400, typeof(object), "Internal error")]
        [SwaggerResponse(404, typeof(Contracts.Customer), "Invalide email format in customer email field")]
        [HttpPost("")]
        public async Task<IActionResult> AddCustomer([FromBody] Contracts.Customer customer)
        {
            if(!IsValidEmail(customer.Email))
            {
                return NotFound(customer);
            }

            var result = await _customerService.AddNewCustomer(Mapper.Map<Models.Customer>(customer));

            return result ? (IActionResult)Ok(new object()) : BadRequest(new object());
        }


        private bool IsValidEmail(string strIn)
        {
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (string.IsNullOrEmpty(strIn))
                return false;

            // Return true if strIn is in valid email format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                return string.Empty;
            }
            return match.Groups[1].Value + domainName;
        }
    }
}
